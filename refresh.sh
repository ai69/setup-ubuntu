#!/bin/bash

#   Date : 2021-05-11 15:08:20 CST

set -eux
set -o pipefail

DIR=$(cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P)
cd "$DIR"

wget -q -O install-oh-my-zsh.sh https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh
chmod +x install-oh-my-zsh.sh

wget -q -O install-outline.sh https://raw.githubusercontent.com/Jigsaw-Code/outline-server/master/src/server_manager/install_scripts/install_server.sh
chmod +x install-outline.sh

git clone --depth=1 https://github.com/zsh-users/zsh-autosuggestions.git
tar czf zsh-autosuggestions.tgz zsh-autosuggestions
rm -fr zsh-autosuggestions

git clone --depth=1 https://github.com/romkatv/powerlevel10k.git
tar czf powerlevel10k.tgz powerlevel10k
rm -fr powerlevel10k

git clone --depth 1 https://github.com/junegunn/fzf.git
tar czf fzf.tgz fzf
rm -fr fzf

git clone --depth 1 https://github.com/dylanaraps/neofetch.git
tar czf neofetch.tgz neofetch
rm -fr neofetch

curl -s -S -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer -o install-pyenv.sh
chmod +x install-pyenv.sh
