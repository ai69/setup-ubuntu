# osu

**O**ne-click **S**etup-script for **U**buntu

> If you're going through hell, keep going.

## For Ubuntu LTS

while inside docker container, install git first:

```bash
apt update && apt install -y git
```

install:

with all in one script ---

```bash
# Normal
cd ~; pwd ; rm -fr ~/.osu ; git clone --depth=1 https://bitbucket.org/ai69/setup-ubuntu.git ~/.osu && cd ~/.osu && ./init-one.sh

# For Azure VM with SSH Keys
cd ~; pwd ; rm -fr ~/.osu ; git clone --depth=1 https://bitbucket.org/ai69/setup-ubuntu.git ~/.osu && cd ~/.osu && ./init-one.sh --init-ext --no-proxy
```

## For elementary OS

install on elementary OS 6.1 Jólnir:

```bash
apt install -y git tmux software-properties-common

# for root
cd ~; pwd ; rm -fr ~/.osu ; git clone https://bitbucket.org/ai69/setup-ubuntu.git ~/.osu && cd ~/.osu && ./init-2004-elem.sh
# then for user
cd ~; pwd ; rm -fr ~/.osu ; git clone https://bitbucket.org/ai69/setup-ubuntu.git ~/.osu && cd ~/.osu && ./init-2004-user.sh
```

## Maintenance

update scripts in repo:

```bash
./refresh.sh
```

remove existing for zen tools

```bash
apt remove -y --purge bat glow dive fd ripgrep hexyl duf && apt clean && apt update
```

## Outline

```bash
export SB_PUBLIC_IP=abcde.japaneast.cloudapp.azure.com
export SB_API_PORT=12345
bash ./install-outline.sh --hostname "$SB_PUBLIC_IP" --api-port "$SB_API_PORT"
```

## Azure apt sources

```bash
# Ubuntu 22.04 LTS
mv sources/sources.list.ubuntu2204 /etc/apt/sources.list
# Ubuntu 20.04 LTS
mv sources/sources.list.ubuntu2004 /etc/apt/sources.list
# Ubuntu 18.04 LTS
mv sources/sources.list.ubuntu1804 /etc/apt/sources.list
```

## TODO

- alias for hash methods
- clean up after install
- use wget/curl instead of manual git
- Add gpg-agent git-lfs neofetch for 16.04
