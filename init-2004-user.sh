#!/bin/bash

set -xeuo pipefail

CWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
cd "$CWD"

export DEBIAN_FRONTEND=noninteractive

# oh-my-zsh and plugins
ZSH_RT=~/.oh-my-zsh
[ ! -d "$ZSH_RT" ] && zsh ./install-oh-my-zsh.sh --unattended

ZSH_AS=${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
tar xzf zsh-autosuggestions.tgz && rm -fr "$ZSH_AS" && mv zsh-autosuggestions "$ZSH_AS"

ZSH_PL=${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
tar xzf powerlevel10k.tgz && rm -fr "$ZSH_PL" && mv powerlevel10k "$ZSH_PL"

chsh -s "$(which zsh)"

# deploy dotfiles
cp -rv dotfiles/. ~/
HOME=$(eval echo "~")
sed -i "s,/root,$HOME,g" "$HOME/.zshrc"

# git init
git lfs install
git config --global core.pager "delta"

# fzf
rm -fr ~/.fzf; tar xzf fzf.tgz && mv -v fzf ~/.fzf && ~/.fzf/install --bin --completion

echo "🦋 Done"
