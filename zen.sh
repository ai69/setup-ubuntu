#!/bin/bash

TEMP_DIR="/tmp"
DEST_DIR="/usr/local/bin"
CUR_DIR=$(cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P)
cd "$CUR_DIR"

function check_missing() {
    local APP="$1"
    if ! [ -x "$(command -v $APP)" ]; then
        return 0 # true
    else
        echo "👌 '$APP' has already been installed"
        return 1 # false
    fi
}

function check_installed() {
    local APP="$1"
    if [ -x "$(command -v $APP)" ]; then
        echo "✅ '$APP' is just installed"
    else
        echo "🚨 failed to install '$APP'"
        exit 1
    fi
}

function install_direct() {
    local APP="$1"
    local URL="$2"
    check_missing "$APP" && (
        wget -cqL "$URL" -O "$APP"
        chmod +x "$APP" && mv -v "$APP" "$DEST_DIR"
    ) && check_installed "$APP"
    return 0
}

function install_zip() {
    local APP="$1"
    local URL="$2"
    local TARGET="$TEMP_DIR"/"$APP".zip
    check_missing "$APP" && (
        wget -cqL "$URL" -O "$TARGET"
        unzip -oqj "$TARGET"
        chmod +x "$APP" && mv -v "$APP" "$DEST_DIR"
    ) && check_installed "$APP"
    return 0
}

function install_tgz() {
    local APP="$1"
    local URL="$2"
    local TARGET="$TEMP_DIR"/"$APP".tgz
    check_missing "$APP" && (
        wget -cqL "$URL" -O "$TARGET"
        tar zxf "$TARGET"
        chmod +x "$APP" && mv -v "$APP" "$DEST_DIR"
    ) && check_installed "$APP"
    return 0
}

function install_deb() {
    local APP="$1"
    local URL="$2"
    local TARGET="$TEMP_DIR"/"$APP".deb
    check_missing "$APP" && (
        wget -cqL "$URL" -O "$TARGET"
        dpkg -i "$TARGET"
    ) && check_installed "$APP"
    return 0
}

function install_apt() {
    local APP="$1"
    local NAME="$2"
    check_missing "$APP" && (
        apt-get install -y --no-install-recommends "$NAME"
    ) && check_installed "$APP"
    return 0
}

# starcli beta: https://github.com/PureMature/starcli
install_direct starcli https://github.com/PureMature/starcli/releases/download/b5/starcli.linux

# ctop:
install_direct "ctop" "https://github.com/bcicen/ctop/releases/download/0.7.6/ctop-0.7.6-linux-amd64"

# jq: https://github.com/stedolan/jq
install_direct "jq" "https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64"

# jid: https://github.com/simeji/jid
install_zip "jid" "https://github.com/simeji/jid/releases/download/v0.7.6/jid_linux_amd64.zip"

# bat: https://github.com/sharkdp/bat
install_deb "bat" "https://github.com/sharkdp/bat/releases/download/v0.19.0/bat_0.19.0_amd64.deb"

# duf: https://github.com/muesli/duf
install_deb "duf" "https://github.com/muesli/duf/releases/download/v0.8.1/duf_0.8.1_linux_amd64.deb"

# glow: https://github.com/charmbracelet/glow
install_deb "glow" "https://github.com/charmbracelet/glow/releases/download/v1.4.1/glow_1.4.1_linux_amd64.deb"

# gum: https://github.com/charmbracelet/gum
install_deb "gum" "https://github.com/charmbracelet/gum/releases/download/v0.4.0/gum_0.4.0_linux_amd64.deb"

# dive: https://github.com/wagoodman/dive
install_deb "dive" "https://github.com/wagoodman/dive/releases/download/v0.10.0/dive_0.10.0_linux_amd64.deb"

# tldr: https://github.com/isacikgoz/tldr
install_tgz "tldr" "https://github.com/isacikgoz/tldr/releases/download/v1.0.0-alpha/tldr_1.0.0-alpha_linux_amd64.tar.gz"

# navi: https://github.com/denisidoro/navi
install_tgz "navi" "https://github.com/denisidoro/navi/releases/download/v2.19.0/navi-v2.19.0-x86_64-unknown-linux-musl.tar.gz"

# lazydocker: https://github.com/jesseduffield/lazydocker
install_tgz "lazydocker" "https://github.com/jesseduffield/lazydocker/releases/download/v0.12/lazydocker_0.12_Linux_x86_64.tar.gz"

# fd: https://github.com/sharkdp/fd
install_deb "fd" "https://github.com/sharkdp/fd/releases/download/v8.3.2/fd_8.3.2_amd64.deb"

# Broot: https://github.com/Canop/broot
install_zip "broot" "https://github.com/Canop/broot/releases/download/v1.9.2/broot_1.9.2.zip"

# ripgrep: https://github.com/BurntSushi/ripgrep
install_deb "rg" "https://github.com/BurntSushi/ripgrep/releases/download/13.0.0/ripgrep_13.0.0_amd64.deb"

# bandwhich: https://github.com/imsnif/bandwhich
install_tgz "bandwhich" "https://github.com/imsnif/bandwhich/releases/download/0.20.0/bandwhich-v0.20.0-x86_64-unknown-linux-musl.tar.gz"

# hexyl: https://github.com/sharkdp/hexyl
install_deb "hexyl" "https://github.com/sharkdp/hexyl/releases/download/v0.9.0/hexyl_0.9.0_amd64.deb"
