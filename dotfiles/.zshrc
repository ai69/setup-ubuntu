# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
    source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Attempt to be Zsh native
export TZ='Asia/Shanghai'

export GOROOT=/usr/local/go
export GOPATH=/root/go
export GOBIN=$GOPATH/bin
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin

# Path to your oh-my-zsh installation.
export ZSH="/root/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
# ZSH_THEME="geoffgarside"
# ZSH_THEME="ys"
ZSH_THEME="powerlevel10k/powerlevel10k"

# Fix: Insecure completion-dependent directories detected
ZSH_DISABLE_COMPFIX="true"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git tmux docker encode64 extract autojump fzf z colorize zsh-autosuggestions)

source $ZSH/oh-my-zsh.sh

# User configuration
export PATH=$PATH:$HOME/.iterm2

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Terminal
export TERM=xterm-256color

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

alias pwdgen='kpwdgen -l 15 -s 1 -d 1'

function gen_sshkey() {
    local pkpath=~/.ssh/id_rsa
    if [[ ! -f "$pkpath" ]]; then
        ssh-keygen -t rsa -b 4096 -N '' -f "$pkpath"
    fi
    cat "$pkpath".pub
}

function mcd() { mkdir -p "$1" && cd "$1"; }

function cdf() {
    if [[ -d "$1" ]]; then
        cd "$1"
    else
        cd $(dirname "$1")
    fi
}

function jv() { < "$1" jq -C . | less -R }

function mktgz() {
    if [[ -z "$1" ]]; then
        echo "missing source target"
        return 1
    else
        tar cvzf "${1%%/}.tgz" "${1%%/}/"
    fi
}

function mkzip() {
    if [[ -z "$1" ]]; then
        echo "missing source target"
        return 1
    else
        zip -r "${1%%/}.zip" "$1"
    fi
}

function mktar() {
    if [[ -z "$1" ]]; then
        echo "missing source target"
        return 1
    else
        tar cvf "${1%%/}.tar" "${1%%/}/"
    fi
}

function run() {
    chmod +x "$1" && ./"$1"
}

function renamehost() {
    [[ -z "$1" ]] && echo "missing name" && return 1
    hostnamectl set-hostname "$1"
}

function dl() {
    case $# in
        2) curl -L -o "$2" "$1" ;;
        1) curl -L -O "$1" ;;
        0) echo "missing url";;
        *) echo "wtf?"
    esac
}

function tfind() {
    if [[ -z "$1" ]]; then
        echo "missing target keyword"
        return 1
    fi
    local words=("$@")
    echo "Searching for '${words[*]}'"
    find . -type f \( -name '*.txt' -o -name '*.md' \) -print0 | xargs -0 -I{} grep -H -i "${words[*]}" {} | sed 's/:/\t/'
}

alias l='ls -CF'
alias la='ls -A'
alias ll='ls -lashF'
alias ls='ls --color=auto'
alias lsp="find . -type f -not -path '*/\.git/*' | sed 's/^\.\///g' | sort"
alias lsmax="find . -type f -not -path '*/\.git/*' -printf '%s %p\n' | sort -nr | head -10"
alias lslast="find . -type f -not -path '*/\.git/*' -printf '%TY-%Tm-%Td %TT %p\n' | sort -nr | head -10"

alias pd='basename "$PWD"'
alias sha1dir='find . -type f -print0  | xargs -0 sha1sum'
alias trim="awk '{\$1=\$1;print}'"
alias pc=it2copy

alias dk=docker
alias dkc='docker container'
alias dkcm=docker-compose
alias dkimg='docker image ls'
alias dklg='docker logs -f'
alias dkls='docker ps -a'
alias dkps='docker ps -a'
alias dkrm='docker rm -f'
alias dks='docker service'
alias dksm='docker swarm'
alias dkst='docker stack'
alias dkstat='docker system df'

function dkclear() {
    docker system prune -f || echo "failed to prune system"
    docker image prune -f || echo "failed to prune images"
    docker ps -a | grep Exit | cut -d ' ' -f 1 | xargs docker rm
}

function dkremoveall() {
    docker rm -vf $(docker ps -a -q) || echo "failed to remove all containers"
    docker rmi -f $(docker images -a -q) || echo "failed to remove all images"
    docker system prune -a --volumes -f || echo "failed to prune system"
}

alias t=tmux
alias ts='tmux ls'
alias ta='tmux attach -t'
alias tag='tmux attach -t github'
alias getip='kgetip --outbound --public'
alias getloc='kgetip --outbound --public --location'

function getos() {
    if [ -f /etc/os-release ]; then
        source /etc/os-release
        OS=$NAME
        VER=$VERSION_ID
        printf "✅ OS: %s %s\n" "$OS" "$VER"
    else
        echo "🥩 Unsupported OS"
        exit 1
    fi
}

function pcm2wav() {
    if [[ $# -lt 1 ]]; then
        echo "Usage: pcm2wav <pcm_file>"
        return 1
    fi
    local fs="$1"
    local ft="${1%.*}.wav"
    printf "convert ${fs} to ${ft}\n"
    sox -t raw -r 16000 -e signed -b 16 -c 1 "$fs" "$ft"
}

function video2wav() {
    if [[ $# -lt 1 ]]; then
        echo "Usage: video2wav <mp4_file>"
        return 1
    fi
    local fs="$1"
    local ft="${1%.*}.wav"
    printf "convert ${fs} to ${ft}\n"
    ffmpeg -i "$fs" -acodec pcm_s16le -ac 1 -ar 16000 -f wav "$ft"
}

function set_hostname() {
    if [[ $# -lt 1 ]]; then
        echo "Usage: set_hostname <hostname>"
        return 1
    fi
    hostnamectl set-hostname "$1"
}

alias b64e='base64'
alias b64d='base64 -d'

function tn() {
    if [[ -z "$1" ]]; then
        tmux new-session
    else
        tmux new-session -s "$1"
    fi
}

for ((i = 0; i <= 32; i++)); do
    alias ta"$i"="tmux attach -t $i"
done

alias now='TZ="Asia/Shanghai" date "+%Y-%m-%d %H:%M:%S %Z"'
alias utcnow='TZ="UTC" date "+%Y-%m-%d %H:%M:%S %Z"'
alias upgrade='sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y && sudo apt-get autoremove -y'
alias sm='setopt NO_NOMATCH'

function git_author() {
    printf "Git Name  : %s\n" "$(git config --get user.name)"
    printf "Git Email : %s\n" "$(git config --get user.email)"
}

function new_bash() {
    local TARGET=test.sh
    if [[ ! -z "$1" ]]; then
        TARGET="$1"
    fi
    cat <<END >"$TARGET"
#!/bin/bash

#   Date : $(TZ="Asia/Shanghai" date "+%Y-%m-%d %H:%M:%S %Z")
# Author : $(git config --get user.name) <$(git config --get user.email)>
# System : $(python3 -c "import platform; print(platform.system() + ' ' + platform.release())")

set -eux
set -o pipefail

SOURCE="\${BASH_SOURCE[0]:-\${(%):-%x}}"
DIR=\$(cd -P -- "\$(dirname -- "\${SOURCE}")" && pwd -P)
cd "\$DIR"

END
    chmod +x "$TARGET"
}

# makefile
alias m='make'
alias mb='make build'
alias mi='make install'
alias mr='make run'
alias mt='make test'
alias mp='make preview'

# git
alias g=git
alias qg=git
alias gs='git status -s'
alias gtl='git tag -l --sort=-creatordate --format="%(refname:short)%09%09%(creatordate:short)%09%(contents:lines=6)"'
alias gpre='git status ; git add -A ; git diff --staged -w'
alias gmd='git checkout master && git pull && git remote prune origin'
alias gps1='git push --set-upstream origin $(git symbolic-ref -q --short HEAD)'
alias git_undo_last='git reset --soft HEAD~1'
alias dif='git diff --no-index'
alias gd='git diff --no-index'

function git_date_commit() {
    date -d "$1" "+%Y-%m-%d %H:%M:%S %Z"
    test $? -eq 0 || return 1
    GIT_AUTHOR_DATE="$1" GIT_COMMITTER_DATE="$GIT_AUTHOR_DATE" git commit -a -m "$2"
}

function git_corb() {
    if [[ -z "$1" ]]; then
        echo "missing remote branch name for 'git checkout remote branch'"
        return 1
    else
        git checkout -b "$1" origin/"$1"
    fi
}

function git_ignore() {
    local name=.gitignore
    if [[ -z "$1" ]]; then
        local curdir="$(pwd)"
        echo "/${curdir##*/}" >>"$name"
    else
        echo "$1" >>"$name"
    fi
    git add "$name"
}

function git_readme() {
    local name=README.md
    if [[ -z "$1" ]]; then
        local curdir="$(pwd)"
        echo "# ${curdir##*/}" >>"$name"
    else
        echo "$1" >>"$name"
    fi
    git add "$name"
}

function git_init() {
    git init
    git config --get user.name
    git config --get user.email
    git add -A
    if [[ ! -z "$1" ]]; then
        git commit -m "$1"
    fi
}

# golang
function gfmt() {
    gofmt -l -w .
    goreturns -l -w .
    goimports -l -w .
}

alias gdoc='go doc -all .'
alias gdocw='godoc -http=localhost:6060'
alias glist='go list -m -u all'
alias glistj='go list -m -json all'

alias grun='go run -v .'
alias gbuild='go build -ldflags "-s -w" -trimpath -v .'
alias gbuildmac='GOOS=darwin GOARCH=amd64 go build -ldflags "-s -w" -trimpath -v .'
alias gbuildlinux='GOOS=linux GOARCH=amd64 go build -ldflags "-s -w" -trimpath -v .'
alias gbuildwindows='GOOS=windows GOARCH=amd64 go build -ldflags "-s -w" -trimpath -v .'
alias gtest='go test -v -race -cover -covermode=atomic -count 1 .'
alias gbench='go test -parallel=4 -run="none" -benchtime="2s" -benchmem -bench=.'

alias gm='go mod'
alias gmi='go mod init'
alias gmt='go mod tidy'
alias gmtv='time go mod tidy -v'
alias gmg='go mod graph'
alias gmtag='TZ=UTC git --no-pager show --quiet --abbrev=12 --date="format-local:%Y%m%d%H%M%S" --format="v0.0.0-%cd-%h"'
alias gmc='go clean --modcache'
alias gga='go get -v -t -d -insecure ./...'
alias gg='go get -v -t -d -insecure'

# starlark
alias sl='starcli -o since -g -r -I ~/.script/starlark'
function slc() {
    if [ $# -lt 1 ]; then
        echo "Usage: slc <code>"
        return 1
    fi
    local code=("$@")
    starcli -o since -g -r -I ~/.script/starlark -c "${code[*]}"
}

# proxy
function use_noproxy() {
    unset http_proxy
    unset HTTP_PROXY
    unset https_proxy
    unset HTTPS_PROXY
}

function use_proxy() {
    local PROXY='http://172.16.1.135:3128/'
    export http_proxy="$PROXY"
    export HTTP_PROXY="$PROXY"
    export https_proxy="$PROXY"
    export HTTPS_PROXY="$PROXY"
}

alias git_proxy='git config --global http.proxy "http://172.16.1.135:3128"'
alias git_unproxy='git config --global --unset http.proxy'

# common tools
alias dufn="duf -hide-fs overlay,tmpfs -hide-mp '*/kubelet/*'"
alias port='netstat -tuwlanp | grep --color=always -e "^" -e ":[0-9]*\s"'
alias portlis='netstat -tuwlanp | grep "LISTEN" | grep --color=always -e "^" -e ":[0-9]*\s"'

# strange tools
function figtitle() {
    local COL=80
    python -c "print(\"#\"*$COL)"
    figlet -c -w $COL "$@" | sed '/^[[:space:]]*$/d'
    python -c "print(\"#\"*$COL)"
}

function figbig() {
    local COL=120
    figlet -w $COL -f big "$@"
}

function figslant() {
    local COL=120
    figlet -w $COL -f slant "$@"
}

# notify
alias ding='barkme send -b "Ping from $(hostname)" -r glass'
alias cronlog='tail -n 50 -f /var/log/syslog | grep CRON'

# load more
if [ -f ~/.zshrc_own ]; then
    source ~/.zshrc_own
fi

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Bug fix
compdef -d mcd
export EDITOR=vim
