#!/bin/bash

set -euo pipefail

CWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
cd "$CWD"

export DEBIAN_FRONTEND=noninteractive

apt-get update

# required for zsh
apt-get install -y --no-install-recommends zsh curl wget git

# oh-my-zsh and plugins
ZSH_RT=~/.oh-my-zsh
[ ! -d "$ZSH_RT" ] && zsh ./install-oh-my-zsh.sh --unattended

ZSH_AS=${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
tar xzf zsh-autosuggestions.tgz && rm -fr "$ZSH_AS" && mv zsh-autosuggestions "$ZSH_AS"

ZSH_PL=${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
tar xzf powerlevel10k.tgz && rm -fr "$ZSH_PL" && mv powerlevel10k "$ZSH_PL"

chsh -s "$(which zsh)"

# deploy dotfiles
cp -rv dotfiles/. ~/

# developer toolchain
add-apt-repository -y ppa:aos1/diff-so-fancy
apt update
apt-get install -y --no-install-recommends \
    apt-utils software-properties-common apt-transport-https ca-certificates \
    vim tmux time less openssh-client gpg-agent git-lfs diff-so-fancy unrar zip unzip p7zip-full \
    htop iftop sysstat iotop glances rclone rsync vnstat ncdu fail2ban lynx tzdata autojump tig whois \
    iproute2 iputils-ping iputils-tracepath dnsutils net-tools ngrep netcat telnet httpie aria2 \
    gcc g++ build-essential libssl-dev libffi-dev imagemagick cowsay lolcat figlet sox

# set timezone
export TZ='Asia/Shanghai'
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ >/etc/timezone

# git init
git lfs install
git config --global core.pager "diff-so-fancy | less --tabs=4 -RFX"

# azure tools
wget -q -O az.tgz https://aka.ms/downloadazcopy-v10-linux && tar -vzxf az.tgz
AZ_DIR=$(find -name "azcopy_linux_amd64_10*" -type d | head -n 1)
[ -d "$AZ_DIR" ] && mv -v "$AZ_DIR"/azcopy /usr/local/bin && rm -fr "$AZ_DIR" az.tgz

# zen utilities and tools
source zen.sh

# delta: https://github.com/dandavison/delta
install_deb "delta" "https://github.com/dandavison/delta/releases/download/0.11.3/git-delta_0.11.3_amd64.deb"

# neofetch
rm -fr neofetch; tar xzf neofetch.tgz && cd neofetch && make install
cd "$CWD"

# fzf
rm -fr ~/.fzf; tar xzf fzf.tgz && mv -v fzf ~/.fzf && ~/.fzf/install --bin --completion

# python
add-apt-repository ppa:deadsnakes/ppa -y && apt-get update
apt-get install -y --no-install-recommends python3-dev python3-pip python3-setuptools python3.8-distutils python3.8-dev
python3.8 -m easy_install pip
# python3.8 -m pip install -U pip setuptools

# go
GO_VERSION=1.17.8
OS=linux
ARCH=amd64
GO_HASH=980e65a863377e69fd9b67df9d8395fd8e93858e7a24c9f55803421e453f4f99
wget -q -O go.tgz "https://go.dev/dl/go${GO_VERSION}.${OS}-${ARCH}.tar.gz" && echo "$GO_HASH *go.tgz" | sha256sum -c -
rm -fr /usr/local/go && tar -C /usr/local -xzf go.tgz && rm -f go.tgz

echo "🪀 Done"
