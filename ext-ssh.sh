#!/bin/bash

#   Date : 2022-05-28 13:47:35 CST

set -eu
set +o pipefail

DIR=$(cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P)
cd "$DIR"

echo "🧩 Aloha! (*´▽｀)ノノ"
source /etc/os-release
OS=$NAME
VER=$VERSION_ID

# Ensure it's running with root privileges
if [ "$EUID" -ne 0 ]; then
    echo "⛔ Please run as root"
    exit 1
fi

# Allow root to login
SSH_CONFIG_FILE="/etc/ssh/sshd_config"
if grep -q -E "^PermitRootLogin\s+yes" "$SSH_CONFIG_FILE"; then
    echo "🔓 Root login is already allowed"
else
    echo "🔓 Allowing root login"
    sed -i -E "s/^#*\s*PermitRootLogin\s+.*/PermitRootLogin yes/" "$SSH_CONFIG_FILE"
    sed -i -E "s/^#*\s*PubkeyAuthentication\s+.*/PubkeyAuthentication yes/" "$SSH_CONFIG_FILE"
    sed -i -E "s/^#*\s*PasswordAuthentication\s+.*/PasswordAuthentication yes/" "$SSH_CONFIG_FILE"
    sed -i -E "s/^#*\s*StrictModes\s+.*/StrictModes yes/" "$SSH_CONFIG_FILE"
    sed -i -E "s/^#*\s*MaxAuthTries\s+.*/MaxAuthTries 5/" "$SSH_CONFIG_FILE"
fi
cat "$SSH_CONFIG_FILE"
service ssh reload

# Give root privileges to all other user with SSH keys
TMP_FILE="temp_keys"
rm -f "$TMP_FILE"
for X in $(cut -f6 -d ':' /etc/passwd | sort | uniq); do
    if [ -s "${X}/.ssh/authorized_keys" ]; then
        FILE="${X}/.ssh/authorized_keys"
        CNT=$(wc <"$FILE" -l)
        echo "Find ${CNT} key(s) under ${X}"
        if [ "$X" = "/root" ]; then
            # esp. for azure hack: no-port-forwarding,no-agent-forwarding,no-X11-forwarding,command="echo 'Please login as the user \"dev\" rather than the user \"root\".';echo;sleep 10;exit 142"
            grep -v -E "exit\s+" "$FILE" | cat >>"$TMP_FILE"
        else
            cat "$FILE" >>"$TMP_FILE"
        fi
    fi
done

[ ! -f /root/.ssh ] && mkdir -p /root/.ssh
if [ -s "$TMP_FILE" ]; then
    ROOT_KEY_FILE="/root/.ssh/authorized_keys"
    if [ -s "$ROOT_KEY_FILE" ]; then
        cp "$ROOT_KEY_FILE" "${ROOT_KEY_FILE}.bak"
    fi
    cat "$TMP_FILE" | sort | uniq >"$ROOT_KEY_FILE"
    CNT=$(wc <"$ROOT_KEY_FILE" -l)
    echo "🔑 Now got ${CNT} key(s) in root: ${ROOT_KEY_FILE}"
else
    echo "🔑 Skip since no other keys was found."
fi

# Enable BBR
if [[ "$VER" == "16.04" ]]; then
    echo "🚀 BBR is not supported on $VER"
else
    SYSCTL_CONF="/etc/sysctl.conf"
    if grep -q -E "^net\.core\.default_qdisc\s*=\s*fq$" "$SYSCTL_CONF"; then
        echo "🚀 BBR is already enabled"
    else
        echo "🚀 Enabling BBR"
        echo "net.core.default_qdisc=fq" >>"$SYSCTL_CONF"
        echo "net.ipv4.tcp_congestion_control=bbr" >>"$SYSCTL_CONF"
        sysctl -p
        sysctl net.ipv4.tcp_available_congestion_control
        lsmod | grep bbr
    fi
fi

# Install Docker
if [ ! -f /.dockerenv ]; then
    bash ./install-docker.sh || echo "Failed to install Docker"
fi

set -o pipefail
