#!/bin/bash

set -eo pipefail

CWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
cd "$CWD"

# Gets the command name without path
cmd() { echo $(basename $0); }

# Help command output
usage() {
    echo "\
$(cmd) [OPTION...]
--skip-go; Do not install Go Dev environment
--skip-python; Do not install Python Dev environment
--skip-zen; Do not install Zen utilities like bat, jq, ctop
--init-ext; Extensive initialization for SSH and Docker
--no-proxy; Do not use proxy even if it's set in environment or exists
--key; Access Key
-v, --verbose; Enable verbose output
" | column -t -s ";"
}

# Error message
error() {
    echo "$(cmd): invalid option -- '$1'"
    echo "Try '$(cmd) -h' for more information."
    exit 1
}

VERBOSE=0
SKIP_GO_INSTALL=false
SKIP_PYTHON_INSTALL=false
SKIP_ZEN_INSTALL=false
INIT_EXT=false
NO_PROXY=false
ACCESS_KEY=

# For cmd options
opts="vhk:"
for pass in 1 2; do
    while [ -n "$1" ]; do
        case $1 in
        --)
            shift
            break
            ;;
        -*) case $1 in
            -k | --key)
                ACCESS_KEY=$2
                shift
                ;;
            --skip-go) SKIP_GO_INSTALL=true ;;
            --skip-python) SKIP_PYTHON_INSTALL=true ;;
            --skip-zen) SKIP_ZEN_INSTALL=true ;;
            --init-ext) INIT_EXT=true ;;
            --no-proxy) NO_PROXY=true ;;
            -v | --verbose) VERBOSE=$(($VERBOSE + 1)) ;;
            -h | --help)
                usage
                exit 0
                ;;
            --*) error $1 ;;
            -*) if [ $pass -eq 1 ]; then
                ARGS="$ARGS $1"
            else error $1; fi ;;
            esac ;;
        *) if [ $pass -eq 1 ]; then
            ARGS="$ARGS $1"
        else error $1; fi ;;
        esac
        shift
    done
    if [ $pass -eq 1 ]; then
        ARGS=$(getopt $opts $ARGS)
        if [ $? != 0 ]; then
            usage
            exit 2
        fi
        set -- $ARGS
    fi
done

if [ -n "$*" ]; then
    echo "$(cmd): Extra arguments -- $*"
    echo "Try '$(cmd) -h' for more information."
    exit 1
fi

# Display flag result
if [ $VERBOSE -gt 0 ]; then
    echo "Verbosity level: $VERBOSE"
    set -x
fi
if [ ! -z $ACCESS_KEY ]; then
    echo "Access Key: $ACCESS_KEY"
fi
if [ $SKIP_GO_INSTALL = true ]; then
    echo "Skipping Go Dev Environment"
fi
if [ $SKIP_PYTHON_INSTALL = true ]; then
    echo "Skipping Python Dev Environment"
fi
if [ $SKIP_ZEN_INSTALL = true ]; then
    echo "Skipping Zen Utilities"
fi
if [ $INIT_EXT = true ]; then
    echo "Extensive initialization"
fi
if [ $NO_PROXY = true ]; then
    echo "Use no proxy"
fi

set -u

echo "🛫 Cabin crew, prepare for take-off."
INS_DATE=$(TZ='Pacific/Honolulu' date '+%Y-%m-%dT%T%z')
GIT_TAG=$(git describe --tags --dirty --always) || GIT_TAG="ʻike ʻole"
HOST_NAME=$(hostname)
echo "INFO: $HOST_NAME | $GIT_TAG | $INS_DATE | $(pwd)"

# Internal Only
if [ $NO_PROXY = false ]; then
    export PROXY='http://172.16.1.135:3128/'
    export http_proxy="$PROXY"
    export HTTP_PROXY="$PROXY"
    export https_proxy="$PROXY"
    export HTTPS_PROXY="$PROXY"

    set +e
    wget -q --connect-timeout=3s --tries=2 --no-dns-cache --spider 'https://www.google.com/'
    if [ $? = 0 ]; then
        echo "🪐 Proxy is working: ${PROXY}"
    else
        echo "🌏 Proxy is NOT working: ${PROXY}"
        unset http_proxy
        unset HTTP_PROXY
        unset https_proxy
        unset HTTPS_PROXY
    fi
    set -e
fi

# One Script for All Ubuntu
if [ -f /etc/os-release ]; then
    source /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
else
    echo "🥩 Unsupported OS"
    exit 1
fi

echo "🍥 Working on: $OS - $VER"
if [ -f /.dockerenv ]; then
    echo "🐳 Also inside Docker ;( No extensive initialization"
    INIT_EXT=false
fi

if [[ "$OS" != "Ubuntu" ]]; then
    echo "😂 Ubuntu Only"
    exit 2
fi

if [[ "$VER" == "16.04" ]] || [[ "$VER" == "18.04" ]]; then
    echo "Skipping Python Dev Environment for ${OS} ${VER}"
    SKIP_PYTHON_INSTALL=true
fi

export DEBIAN_FRONTEND=noninteractive
apt-get update

# required for zsh
apt-get install -y --no-install-recommends zsh curl wget git

# oh-my-zsh and plugins
ZSH_RT=~/.oh-my-zsh
[ ! -d "$ZSH_RT" ] && zsh ./install-oh-my-zsh.sh --unattended

ZSH_AS=${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
tar xzf zsh-autosuggestions.tgz && rm -fr "$ZSH_AS" && mv zsh-autosuggestions "$ZSH_AS"

ZSH_PL=${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
tar xzf powerlevel10k.tgz && rm -fr "$ZSH_PL" && mv powerlevel10k "$ZSH_PL"

chsh -s "$(which zsh)"

# deploy dotfiles, i.e. cp -rv dotfiles/. ~/
find dotfiles -mindepth 1 -maxdepth 1 -type f \( -iname ".*" \) -print0 |
    while IFS= read -r -d '' f; do
        NAME=$(basename "$f")
        SRC=dotfiles/"$NAME"
        TARGET=~/"$NAME"
        if [ -f "$TARGET" ]; then
            grep -q -E '^#{3,}\sDO\sNOT\sOVERWRITE\s#{3,}$' "$TARGET" && continue
        fi
        cp -vf "$SRC" "$TARGET"
    done

# developer toolchain
apt-get install -y --no-install-recommends \
    apt-transport-https apt-utils aria2 autojump build-essential ca-certificates cowsay dnsutils exiftool \
    fail2ban figlet g++ gcc graphviz htop httpie iftop imagemagick iotop iproute2 iputils-ping iputils-tracepath kmod less llvm \
    libffi-dev libssl-dev lolcat lynx net-tools netcat ngrep openssh-client p7zip-full rsync software-properties-common \
    sysstat telnet tig time tmux tzdata unrar unzip vim vnstat whois zip

if [[ "$VER" == "20.04" || "$VER" == "18.04" ]]; then
    add-apt-repository -y ppa:aos1/diff-so-fancy
    apt update
    apt-get install -y --no-install-recommends \
        diff-so-fancy git-lfs glances gpg-agent ncdu rclone sox
    git config --global core.pager "diff-so-fancy | less --tabs=4 -RFX"
elif [[ "$VER" == "22.04" ]]; then
    apt-get install -y --no-install-recommends \
        git-lfs glances gpg-agent ncdu rclone sox
elif [[ "$VER" == "16.04" ]]; then
    echo "Ohh...it's 16.04, got nothing special"
fi

# set timezone
export TZ='Asia/Shanghai'
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ >/etc/timezone

# git init
if [[ "$VER" != "16.04" ]]; then
    git lfs install || echo "😯 git lfs init failed"
fi

# neofetch
rm -fr neofetch
tar xzf neofetch.tgz && cd neofetch && make install
cd "$CWD"

# fzf
rm -fr ~/.fzf
tar xzf fzf.tgz && mv -v fzf ~/.fzf && ~/.fzf/install --bin --completion

# iterm2
rm -fr ~/.iterm2
tar xzf iterm2.tgz && mv -v .iterm2 ~/.iterm2

# azure tools
wget -q -O az.tgz https://aka.ms/downloadazcopy-v10-linux && tar -vzxf az.tgz
AZ_DIR=$(find -name "azcopy_linux_amd64_10*" -type d | head -n 1)
[ -d "$AZ_DIR" ] && mv -v "$AZ_DIR"/azcopy /usr/local/bin && rm -fr "$AZ_DIR" az.tgz

# keiki
KEIKI_DIR=keiki-dist
[ -d "$KEIKI_DIR" ] && rm -fr "$KEIKI_DIR"
git clone --depth=1 https://github.com/1set/keiki-release.git "$KEIKI_DIR"
mv "$KEIKI_DIR"/linux-amd64/k* /usr/local/bin

# zen utilities and tools
if [ $SKIP_ZEN_INSTALL = false ]; then
    source zen.sh
    if [[ "$VER" == "20.04" ]]; then
        # delta: https://github.com/dandavison/delta
        install_deb "delta" "https://github.com/dandavison/delta/releases/download/0.11.3/git-delta_0.11.3_amd64.deb"
    fi
fi

# set welcome message
EMOJI_SHAKA=🤙
echo "${EMOJI_SHAKA} Ua hoʻokomo ʻia kēia mīkini ($HOST_NAME) ma $INS_DATE me setup-ubuntu @ $GIT_TAG ${EMOJI_SHAKA}" >/etc/motd

# python
if [ $SKIP_PYTHON_INSTALL = false ]; then
    add-apt-repository ppa:deadsnakes/ppa -y && apt-get update
    apt-get install -y --no-install-recommends python3.11 python3.11-dev python3.11-distutils python3-pip python3-venv
    apt-get install -y --no-install-recommends libbz2-dev liblzma-dev libncursesw5-dev libreadline-dev libsqlite3-dev libxml2-dev libxmlsec1-dev tk-dev zlib1g-dev
    update-alternatives --install /usr/bin/python python /usr/bin/python3.11 1
    python -V
    # pyenv
    PYENV_DIR=~/.pyenv
    [ ! -d "$PYENV_DIR" ] && zsh ./install-pyenv.sh
    ZSHRC=~/.zshrc
    cat <<END >>"$ZSHRC"

# pyenv
export PYENV_ROOT="\$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="\$PYENV_ROOT/bin:\$PATH"
eval "\$(pyenv init -)"
eval "\$(pyenv virtualenv-init -)"

alias pe=pyenv
alias peg='pyenv global'
alias pei='pyenv install'
alias pev='pyenv version'
alias pevs='pyenv versions'
alias pevv='pyenv virtualenv'
alias pevvs='pyenv virtualenvs'
alias peva='pyenv activate'
alias pevd='pyenv deactivate'
END
fi

# go: https://go.dev/dl/
if [ $SKIP_GO_INSTALL = false ]; then
    GO_VERSION=1.18.7
    OS=linux
    ARCH=amd64
    GO_HASH=6c967efc22152ce3124fc35cdf50fc686870120c5fd2107234d05d450a6105d8
    wget -q -O go.tgz "https://go.dev/dl/go${GO_VERSION}.${OS}-${ARCH}.tar.gz" && echo "$GO_HASH *go.tgz" | sha256sum -c -
    rm -fr /usr/local/go && tar -C /usr/local -xzf go.tgz && rm -f go.tgz
fi

mkdir -p ~/.script/starlark

# extensions
if [ $INIT_EXT = true ]; then
    source ext-ssh.sh
fi

# clean up
apt autoremove -y

echo "🛬 Have a nice stay!"
