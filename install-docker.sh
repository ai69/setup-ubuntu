#!/bin/bash

set -u
set -o pipefail

DIR=$(cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P)
cd "$DIR"

echo "🐳 Docker! (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧"

STDERR_OUTPUT="$(docker info 2>&1 >/dev/null)"
RET=$?
if ((RET == 0)); then
    echo "Docker is already installed."
    exit 0
elif [[ "${STDERR_OUTPUT}" == *"Is the docker daemon running"* ]]; then
    echo "Docker is not running. Please start Docker and try again."
    exit 0
fi

apt-get remove docker docker-engine docker.io containerd runc || echo "Ignore missing docker"
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --yes --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
# fix
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --yes --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
chmod a+r /usr/share/keyrings/docker-archive-keyring.gpg

echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list >/dev/null

apt update
apt-get install -y --no-install-recommends docker-ce docker-ce-cli containerd.io

docker run --rm hello-world

# docker compose: https://github.com/docker/compose
curl -fsSL "https://github.com/docker/compose/releases/download/v2.7.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

docker-compose version
